
// Enable us to simply require('websocket') from within the examples directory
/* old version add to ENV
 * require.paths.unshift('/lib/');
 */

var WebSocketServer = require('websocket').server;
var express = require('express');
var fs = require('fs');

var app = express.createServer();

var internationalization = require('./server_lang').internationalization;
var ClientObj = require('./ClientObj').ClientObj;
var i;
app.configure(function() {
    app.use(express.static("./public"));
    app.set('views', "./");
    app.set('view engine', 'ejs');
});
app.get('/chat', function(req, res) {
	var s = req.headers['accept-language'];
	i = s.substring(0, s.indexOf(","));
    res.render('index', { layout: false });
});
app.listen(5556);


var wsServer = new WebSocketServer({
    httpServer: app,
    
    // Firefox 7 alpha has a bug that drops the
    // connection on large fragmented messages
    fragmentOutgoingMessages: false
});

var connections = [];
var clients = [];
var chatHistoryCommands = JSON.parse(fs.readFileSync('history', "utf8"));

wsServer.on('request', function(request) {
	try{
    var connection = request.accept('chat', request.origin);
    var client = new ClientObj();
    client.IP = connection.remoteAddress;
    client.i18n = i;
    connections.push(connection);
    
    client.ConnID = connections.indexOf(connection);
    clients.push(client);
    
    console.log(connection.remoteAddress + " connected - Protocol Version " + connection.websocketVersion);
    // Send latest 10 history commands to the new client
    connection.sendUTF(JSON.stringify({
        method: "initCommands",
        data: chatHistoryCommands
    }));
    
    // Handle closed connections
    connection.on('close', function() {
        console.log(connection.remoteAddress + " disconnected");
		
        var index = connections.indexOf(connection);
        if (index !== -1) {
            // remove the connection from the pool
            connections.splice(index, 1);
        }
		sendSystemMessageToAll(i18n.sboffline.replace("%s", clients[index].Nick));
		
    });
    
    // Handle incoming messages
    connection.on('message', function(message) {
		var clientId = connections.indexOf(connection);
		if (clientId !== -1) {
	    	i18n = new internationalization(clients[clientId].i18n);
	        if (message.type === 'utf8') {
				var t = new Date();
	            try {
	                var command = JSON.parse(message.utf8Data);
					/* add sent time */
					command.time = CZ(t.getHours())+":"+CZ(t.getMinutes())+":"+CZ(t.getSeconds());
					/* add sender ip */
					command.ip = (connection.remoteAddress.indexOf("192.168.2")==0?"administrator":connection.remoteAddress);
					/* add sender name
					 * command.who = ;
					 */
					if(command.who==""||command.who=="undefined")
						command.who=i18n.anonymous;
					
					/* convert back to string */
					message.utf8Data = JSON.stringify(command);
					
				    if (command.method === 'clear') {
				    	if(clients[clientId].IP.indexOf("192.168.2">=0))
				    		chatHistoryCommands = [];
				    }
				    else if (command.method === 'initChangeNick'){
				    	sendSystemMessageToAll(i18n.sbonline.replace("%s", command.who));
				    	clients[clientId].Nick = command.who;
				    }
				    else if (command.method === 'changeNick') {
				    	sendSystemMessageToAll(i18n.changeto.replace("%s1", clients[clientId].Nick).replace("%s2", command.who));
				        clients[clientId].Nick = command.who;
				    }
				    else if (command.method === 'printMessage') {
						//防洪ok
						if(t.getTime()-clients[clientId].LastSend>=1000){
							clients[clientId].FloodCounter=0;
							addHistoryCommands(command);
				            // rebroadcast command to all clients
							connections.forEach(function(destination) {
								destination.sendUTF(message.utf8Data);
							});
						}
						else if(t.getTime()-clients[clientId].LastSend<1000){
							clients[clientId].LastSend = t.getTime();
							sendSystemMessageToOne(connection, i18n.dont+i18n.floodwater);
							if(clients[clientId].FloodCounter++>10){
								sendSystemMessageToAll(i18n.sbban.replace("%s", clients[clientId].Nick));
								sendSystemDisconnectToOne(connection,"banned");
						        connection.close();
					        }
						}
				    }

					console.log(message.utf8Data);
					clients[clientId].LastSend = t.getTime();
	            }
	            catch(e) {
					console.log(e);
	                // do nothing if there's an error.
	            }
	        }
		}
    });
    

	}catch(e){
		console.log(e.message);
	}
});

var addHistoryCommands = function(command){
	try{
	/* 歷史紀錄只紀錄10筆 */
		if(chatHistoryCommands.length>=10)
			chatHistoryCommands.shift();
		/* history color*/
		command.color="#aaaaaa";
		chatHistoryCommands.push(command);
		fs.writeFileSync('history', JSON.stringify(chatHistoryCommands), "utf8");
		var log = fs.createWriteStream('history.txt', {'flags': 'a'});
	// use {'flags': 'a'} to append and {'flags': 'w'} to erase and write a new file
		log.write(JSON.stringify(command)+"\n");
	}catch(e){
		console.log(e.message);
	}
};

function ArrayIndex(ary,target){
	for(i in ary){
		if(ary[i]==target)
			return i;
	}
}

var sendSystemMessageToAll = function(mesg){
	try{
		connections.forEach(function(destination) {
			var disconnect=new Object();
			disconnect.data=mesg;
			disconnect.who=i18n.system;
			disconnect.time=clientTime();
			disconnect.ip="";
			disconnect.method="printMessage";
			destination.sendUTF(JSON.stringify(disconnect));
		});
	}catch(e){
		console.log(e.message);
	}
};

var sendSystemMessageToOne = function(connTo, mesg){
	try{
		var disconnect=new Object();
		disconnect.data=mesg;
		disconnect.who=i18n.system;
		disconnect.time=clientTime();
		disconnect.ip="";
		disconnect.method="printMessage";
		connTo.sendUTF(JSON.stringify(disconnect));
	}catch(e){
		console.log(e.message);
	}
};

var sendSystemDisconnectToOne = function(connTo, mesg){
	try{
		var disconnect=new Object();
		disconnect.data=mesg;
		disconnect.who=i18n.system;
		disconnect.time=clientTime();
//		disconnect.ip="";
		disconnect.method="disconnect";
		connTo.sendUTF(JSON.stringify(disconnect));
	}catch(e){
		console.log(e.message);
	}
};

var sendOneMessageToOne = function(connFrom, connTo, mesg){
	try{
		var disconnect=new Object();
		disconnect.data=mesg+connFrom;
		disconnect.who=i18n.system;
		disconnect.time=clientTime();
		disconnect.ip="";
		disconnect.method="printMessage";
		connTo.sendUTF(JSON.stringify(disconnect));
	}catch(e){
		console.log(e.message);
	}
};

var SelectCommand = function (command){
};
/* 
 * 自動補0為2位數字
 */
var CZ = function(p) {(p < 10 ? s = "0" + p : s = p);return s;};

var clientTime = function() {
	var t = new Date();
	return CZ(t.getHours()) + ":" + CZ(t.getMinutes()) + ":" + CZ(t.getSeconds());
};
console.log("chat test app ready");
console.log("Listen on port 5556");
