/************************************************************************
 *  Copyright 2010-2011 Worlize Inc.
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 ***********************************************************************/


var i18n = internationalization((navigator.browserLanguage || navigator.language).toLowerCase());
function Chatroom(canvasId) {
	this.url = "";//('https:' == document.location.protocol ? 'wss://' : 'ws://') + document.location.host + ":5556/chat"
	this.logpanel;
	this.nick;
	this.canvasId;
	this.canvas;
	this.message;
	this.sendBtn;
	this.cleanBtn;
	this.bee;
	this.volumeBtn;
	this.connectBtn;
	this.initChatroom(canvasId);
	// Define accepted commands
	this.messageHandlers = {
		initCommands : this.initCommands.bind(this),
		printMessage : this.printMessage.bind(this)
	//        clear: this.clear.bind(this)
	};

};

Chatroom.prototype.initChatroom = function(canvasId) {
	//not support WebSocket Security
	this.url = ('https:' == document.location.protocol ? 'ws://' : 'ws://') + document.location.host + ":5556/chat";
	this.setupStorage();
	this.logpanel = $("#logpanel")[0];
	this.nick = $("#nick")[0];
	this.canvasId = canvasId;
	this.canvas = $("#"+canvasId)[0];
	this.message = $("#message")[0];
	this.sendBtn = $("#sendbtn")[0];
	this.cleanBtn = $("#cleanbtn")[0];
	this.bee = $("#bee")[0];
	this.volumeBtn = $("#volumebtn")[0];
	this.connectBtn = $("#connectbtn")[0];
	
	this.addElementEventListeners();
	this.addElementEffecters();
};
Chatroom.prototype.setupStorage = function(){
	/* handle old Storage to new Storage*/
	if(localStorage.nick!==undefined){
		var MCObj = {'nick':localStorage.nick,'ring':true,'reserve':''};
		localStorage.removeItem('nick');
		localStorage.MCObj = JSON.stringify(MCObj);
	}
	
	if(localStorage.MCObj!==undefined)
		this.MCObj = JSON.parse(localStorage.MCObj);
	else{
		this.MCObj = {'nick':'','ring':true,'reserve':''};
	}
};

Chatroom.prototype.connect = function() {
	var wsCtor = window['MozWebSocket'] ? MozWebSocket : WebSocket;
	this.socket = new wsCtor(this.url, 'chat');

	this.socket.onopen = this.handleWebsocketOpen.bind(this);
	this.socket.onmessage = this.handleWebsocketMessage.bind(this);
	this.socket.onclose = this.handleWebsocketClose.bind(this);
	
};

Chatroom.prototype.handleWebsocketOpen = function(open) {
	this.changeNick(true);
};

Chatroom.prototype.handleWebsocketMessage = function(message) {
	try {
		var command = JSON.parse(message.data);
	} catch (e) { /* do nothing */
	}

	if (command) {
		this.dispatchCommand(command);
	}
};

Chatroom.prototype.handleWebsocketClose = function() {
	this.log(i18n.websocketdisconnect);
	this.toggleOnline(false);
};

Chatroom.prototype.dispatchCommand = function(command) {
	// Do we have a handler function for this command?
	var handler = this.messageHandlers[command.method];
	if (typeof (handler) === 'function') {
		// If so, call it and pass the parameter data
		handler.call(this, command);
	}
};

Chatroom.prototype.initCommands = function(commandList) {
	/* Upon connection, the contents of the chatroom
	   are drawn by replaying all commands since the
	   last time it was cleared */
	commandList.data.forEach(function(command) {
		this.dispatchCommand(command);
	}.bind(this));
	//	this.log("=============歷史訊息=============");
};

Chatroom.prototype.sendClear = function() {
	this.socket.send(JSON.stringify({
		method : 'clear'
	}));
};

Chatroom.prototype.printMessage = function(data) {
	this.log(data);
};

Chatroom.prototype.disconnect = function(data) {
	this.socket.close();
};

Chatroom.prototype.scrollToBottom = function() {
	$(this.logpanel).scrollTop(this.logpanel.scrollHeight);
};

Chatroom.prototype.log = function(command) {
	var newEle;
	var genChatTime = function(cmd, isLocal){
		if(isLocal)
			return "(" + Chatroom.prototype.clientTime() + ") ";
		else
			return "(" + cmd.time + ") ";
	};
	var genChatWho = function(cmd, isLocal){
		if(isLocal)
			return $("<span>").text(cmd+": ");
		else
			return $("<span>").text(cmd.who+": ")
			.mousemove(function(event) {
				$(this).css("cursor", "pointer");
				$("#nick_desc").css({
					"left" : event.pageX + 20,
					"top" : event.pageY + 20
				});
			})
			.mouseover(function() {
				$("#nick_desc").text(cmd.ip).show();
				$(this).css("cursor", "pointer");
			})
			.mouseout(function() {
				$("#nick_desc").hide();
				$(this).css("cursor", "default");
			})
			.click(function(){
				$("#message").val(cmd.who+": ").focus();
			});
	};
	var genChatContent = function(cmd, isLocal){
		if(isLocal)
			return "<span>"+cmd+"</span>";
		else
			return "<span>"+cmd.data+"</span>";
	};
	
	if (typeof (command) == "string") {
		newEle = $("<p>")
			.css({"overflow-x" : "hidden"})
			.append(genChatTime(command, true))
			.append(genChatWho(i18n.system, true))
			.append(genChatContent(command, true));
	}
	else {
		newEle = $("<p>")
			.css({
				color : (command.color == undefined ? "#000000" : command.color),
				"overflow-x" : "hidden"
			})
			.append(genChatTime(command, false))
			.append(genChatWho(command, false))
			.append(genChatContent(command, false));
		this.ring();
	}
	$(this.logpanel).append($("<li>").append(newEle));
	this.scrollToBottom();
};

/*
 Chatroom.prototype.clear = function() {
 this.canvas.width = this.canvas.width;
 };*/

Chatroom.prototype.handleSendBtn = function(e) {
	this.sendmsg();
};

Chatroom.prototype.handleMessageSend = function(e) {
	if (e.which == 13) {
		this.sendmsg();
	}
};


Chatroom.prototype.addElementEventListeners = function() {
	this.sendBtn.addEventListener('click', this.handleSendBtn.bind(this), false);
	this.message.addEventListener('keydown', this.handleMessageSend.bind(this), false);
	
	this.nick.addEventListener('change', this.handleNickChange.bind(this), false);
	this.cleanBtn.addEventListener('click', this.handleCleanBtn.bind(this), false);
	this.volumeBtn.addEventListener('click', this.handleVolumeBtn.bind(this), false);
	this.connectBtn.addEventListener('click', this.handleConnectBtn.bind(this), false);
};

Chatroom.prototype.handleCleanBtn = function(e) {
	$(this.logpanel).children().remove();
	$(this.logpanel).append($("li").css("text-align","center").text("------------- "+i18n.screenclear+" -------------"));
};

Chatroom.prototype.handleVolumeBtn = function(e) {
	var btn = $(this.volumeBtn);
	if($(btn).hasClass("btn-icon-sound")){
		$(btn).removeClass("btn-icon-sound");
		$(btn).addClass("btn-icon-mute");
		$(btn).attr("title", i18n.mute);
		this.MCObj.ring = false;
		localStorage.MCObj = JSON.stringify(this.MCObj);
	}
	else{
		$(btn).removeClass("btn-icon-mute");
		$(btn).addClass("btn-icon-sound");
		$(btn).attr("title", i18n.sound);
		this.MCObj.ring = true;
		localStorage.MCObj = JSON.stringify(this.MCObj);
	}
};
Chatroom.prototype.handleConnectBtn = function(e) {
	this.toggleOnline();
};

Chatroom.prototype.handleNickChange = function(e) {
	if (this.MCObj.nick === undefined || this.MCObj.nick == "undefined"){
		this.MCObj.nick = "";
		localStorage.MCObj = JSON.stringify(this.MCObj);
	}
	else{
		this.MCObj.nick = $(this.nick).val();
		localStorage.MCObj = JSON.stringify(this.MCObj);
	}
	this.changeNick(false);
};

Chatroom.prototype.addElementEffecters = function() {
	$("#nickword")
	.text(i18n.nick);
	$("#msgword")
	.text(i18n.msg);

	$(this.nick)
	.val(this.MCObj.nick);
	
	$(this.sendBtn)
	.attr("title",i18n.send)
	.button();

	$(this.cleanBtn)
/*	.click(function(){
		$(this.logpanel).children().remove();
		$(this.logpanel).append($("li").css("text-align","center").text("------------- "+i18n.screenclear+" -------------"));
	})*/
	.attr("title", i18n.clean)
	.button();
	
	$(this.volumeBtn)
	.addClass("btn-icon ui-widget ui-corner-all")
	.attr("title", i18n.sound);
	if(this.MCObj.ring)
		$(this.volumeBtn).addClass("btn-icon-sound");
	else
		$(this.volumeBtn).addClass("btn-icon-mute");
	
	$(this.connectBtn)
	.addClass("btn-icon btn-icon-online ui-widget ui-corner-all")
	.attr("title", i18n.online);
};

Chatroom.prototype.toggleOnline = function(online) {
	var btn = $(this.connectBtn);
	if(online===undefined && btn.hasClass("btn-icon-online") || online===false){
		btn.removeClass("btn-icon-online");
		btn.addClass("btn-icon-offline");
		btn.attr("title", i18n.offline);
		this.socket.close();
	}
	else{
		btn.removeClass("btn-icon-offline");
		btn.addClass("btn-icon-online");
		btn.attr("title", i18n.online);
		this.connect();
	}
};

Chatroom.prototype.changeNick = function(isInit) {
//	$(this.logpanel).scrollTop = 9999999;
	if (this.socket && this.socket.readyState == 1) {
		var nick = $(this.nick).val();
		//      history.unshift(msg);
		this.socket.send(JSON.stringify({
			"method" : (isInit?'initChangeNick':'changeNick'),
			"who" : nick
		}));
		idx = 0;
		$(this.message).val("");
	} else {
		this.log(i18n.disconnect);
	}
};
Chatroom.prototype.sendmsg = function() {
	$(this.logpanel).scrollTop = 9999999;
	if (this.socket && this.socket.readyState == 1) {
		var msg = $(this.message).val();
		var nick = $(this.nick).val();
		//      history.unshift(msg);
		if (msg != "") {
			this.socket.send(JSON.stringify({
				"method" : 'printMessage',
				"data" : msg,
				"who" : nick
			}));
			idx = 0;
			$(this.message).val("");
		} else {
			this.log(i18n.chattextcannotempty);
		}
	} else {
		this.log(i18n.disconnect);
	}
};
Chatroom.prototype.clientTime = function() {
	var t = new Date();
	var f = function(p) {(p < 10 ? s = "0" + p : s = p);return s;};
	return f(t.getHours()) + ":" + f(t.getMinutes()) + ":" + f(t.getSeconds());
};

Chatroom.prototype.ring = function() {
	if($(this.volumeBtn).hasClass("btn-icon-sound"))
		$("#bee").get(0).play();
};