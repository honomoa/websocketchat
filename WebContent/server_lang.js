
function internationalization(local){
	this.local = local||"zh-tw";
	this.localize = {
			// 正體中文
			'zh-tw' : {
				'mute' : '靜音',
				'sound' : '聲音',
				'send' : '送出',
				'clean' : '清除',
				'nick' : '暱稱',
				'msg' : '訊息',
				'chattextcannotempty' : '訊息不能為空白',
				'disconnect' : '與伺服器斷線',
				'screenclear' : '清除螢幕',
				'websocketdisconnect' : 'WebSocket 連線已中斷',
				'system':'系統',
				'anonymous':'匿名',
				'dont':'不要',
				'floodwater':'灌水',
				'sboffline':'%s 已離線',
				'sbonline':'%s 已上線',
				'offline':'離線',
				'online':'上線',
				'historymsg':'歷史訊息',
				'changeto':'%s1 改名為 %s2',
				'sbban':'%s 被禁止了'
			},
			'en-us' : {
				'mute' : 'Mute',
				'sound' : 'Sound',
				'send' : 'Send',
				'clean' : 'Clean',
				'nick' : 'Nick',
				'msg' : 'MSG',
				'chattextcannotempty' : 'Chat Text Cannot Empty',
				'disconnect' : 'Disconnect on server',
				'screenclear' : 'Screen Clear',
				'websocketdisconnect' : 'WebSocket has been disconnected',
				'system':'System',
				'anonymous':'anony',
				'dont':'dont',
				'floodwater':'flood',
				'sboffline':'%s has been offline',
				'sbonline':'%s is online',
				'offline':'Offline',
				'online':'Online',
				'historymsg':'History Msg',
				'changeto':'%s1 change to %s2',
				'sbban':'%s was banned'
			},
			'en-uk' : {
				'mute' : 'Mute',
				'sound' : 'Sound',
				'send' : 'Send',
				'clean' : 'Clean',
				'nick' : 'Nick',
				'msg' : 'MSG',
				'chattextcannotempty' : 'Chat Text Cannot Empty',
				'disconnect' : 'Disconnect on server',
				'screenclear' : 'Screen Clear',
				'websocketdisconnect' : 'WebSocket has been disconnected',
				'system':'System',
				'anonymous':'anony',
				'dont':'dont',
				'floodwater':'flood',
				'sboffline':'%s has been offline',
				'sbonline':'%s is online',
				'offline':'Offline',
				'online':'Online',
				'historymsg':'History Msg',
				'changeto':'%s1 change to %s2',
				'sbban':'%s was banned'
			},
			'en' : {
				'mute' : 'Mute',
				'sound' : 'Sound',
				'send' : 'Send',
				'clean' : 'Clean',
				'nick' : 'Nick',
				'msg' : 'MSG',
				'chattextcannotempty' : 'Chat Text Cannot Empty',
				'disconnect' : 'Disconnect on server',
				'screenclear' : 'Screen Clear',
				'websocketdisconnect' : 'WebSocket has been disconnected',
				'system':'System',
				'anonymous':'anony',
				'dont':'dont',
				'floodwater':'flood',
				'sboffline':'%s has been offline',
				'sbonline':'%s is online',
				'offline':'Offline',
				'online':'Online',
				'historymsg':'History Msg',
				'changeto':'%s1 change to %s2',
				'sbban':'%s was banned'
			}
		};
	return this.localize[this.local.toLowerCase()];
}

internationalization.prototype.setLocal = function(local){
	this.local = local;
};
module.exports.internationalization = internationalization;